var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.json());


app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header('Access-Control-Allow-Methods', 'GET,POST,PATCH,DELETE');
  next();
});

let backgrounds = [
"https://images.unsplash.com/photo-1514810771018-276192729582?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
"https://secureservercdn.net/160.153.138.219/x82.553.myftpupload.com/wp-content/uploads/2020/05/Caribbean-beach.jpg",
"https://htmlcolorcodes.com/assets/images/html-color-codes-color-tutorials.jpg",
"https://thewallpaper.co//wp-content/uploads/2017/05/mac-backgrounds-foliage-forest-nature-beautiful-nature-forest-plant-trees-widescreen-ultra-hd.jpg"]

let dashboardMock= [{
  background: backgrounds[Math.floor(Math.random() * backgrounds.length)],
  id: 1,
  title: "Мой dashboard"
},
{
  background: backgrounds[Math.floor(Math.random() * backgrounds.length)],
  id: 2,
  title: "НАШ dashboard"
}]

// Для каждого дашбоарда свой ключ с пустым списком карточек
let cardsMock= {1:[],2:[]}


app.get("/getDashboards", (req, res) => {
  res.send(dashboardMock);
});

app.get("/getDashboard", (req, res) => {
  const id = +req.query.id;
  res.send(dashboardMock.find(dashboard => dashboard.id === id));
});

app.patch("/editDashboard", (req, res) => {
  const {dashboard, title} = req.body;
  let editedDashoard = dashboardMock.find((mockedDashboard) => mockedDashboard.id === dashboard.id)
  if (editedDashoard) editedDashoard.title = title;
  res.send({dashboard:editedDashoard, dashboards:dashboardMock});
});

app.get("/addDashboard", (req, res) => {
  const id = dashboardMock.length>0?dashboardMock[dashboardMock.length-1].id+1:1
  dashboardMock.push({background:backgrounds[Math.floor(Math.random() * backgrounds.length)], id:id,title:"Новый Dashboard"})
  cardsMock[id]=[]
  res.send(id.toString());
});

app.get("/deleteDashboard", (req, res) => {
  const id = +req.query.id;
  dashboardMock=dashboardMock.filter(dashboard=>dashboard.id!==id);
  cardsMock[id]=[]
  res.send(dashboardMock);
});

app.get("/getCards", (req, res) => {
  const id = req.query.id;
  res.send(cardsMock[id]);
});

app.post("/addCard", (req, res) => {
  const {card} = req.body;
  const id = card.dashboardId;
  cardsMock[id].push(card);
  res.send(cardsMock[id]);
});

app.post("/deleteCard", (req, res) => {
  const {card} = req.body;
  const dashboardId = card.dashboardId;
  const id = card.id;
  cardsMock[dashboardId]=cardsMock[dashboardId].filter(card=>card.id!==id);
  res.send(cardsMock[dashboardId]);
});

app.patch("/editCard", (req, res) => {
  const {newCard} = req.body
  const id = newCard.dashboardId;
  cardsMock[id] =  cardsMock[id].map(card => {
    if (card.id === newCard.id) {
      card = newCard;
    }
    return card;
  })
  res.send(cardsMock[id]);
});

app.post("/changeCardOrder", (req, res) => {
  const id = req.query.id;
  const {newCardOrder} = req.body
  cardsMock[id] = newCardOrder
  res.send(cardsMock[id]);
});


app.listen(8000, function () {
  console.log('Example app listening on port 8000!');
});
